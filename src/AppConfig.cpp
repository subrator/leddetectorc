#include "AppConfig.hpp"
#include "Defines.hpp"
#include "Environment.hpp"
#include "Directory.hpp"
#include "Json.hpp"
#include <memory.h>
#include <stdio.h>
#include <malloc.h>

static char configuration_path[1024] = { 0 };
static char configuration_file[1024] = { 0 };
static char process_name[65]= {0};

char plugin_device[65];
char plugin_name[65];
char model_file[129];
char label_file[129];
char destination[65];
char device_id[65];
char resource_files[129];
long roi_x1;
long roi_y1;
long roi_x2;
long roi_y2;
double threshold_score;
long callback_frequency;

bool loadConfiguration()
{
    memset(plugin_device, 0, 65);
    memset(plugin_name, 0, 65);
    memset(model_file, 0, 129);
    memset(label_file, 0, 129);
    memset(destination, 0, 65);
    memset(device_id, 0, 65);
    memset(resource_files, 0, 129);
    roi_x1 = 50;
    roi_y1 = 100;
    roi_x2 = 50;
    roi_y2 = 100;
    callback_frequency = 60;
    threshold_score = 0.6;

    env_get_current_process_name(process_name);
    dir_get_config_directory(&configuration_path[0]);

    strcat(configuration_file, configuration_path);
    if(configuration_file[strlen(configuration_file)-1] != '/')
    {
        strcat(configuration_file, "/");
    }
    strcat(configuration_file, process_name);
    strcat(configuration_file, ".json");

    char* buffer = nullptr;
    long sz = 0;

    FILE* fp = fopen(configuration_file, "r");

    if(!fp)
    {
        return false;
    }

    fseek(fp, 0L, SEEK_END);
    sz = ftell(fp);
    rewind(fp);
    sz;

    buffer = (char*)calloc(1, sz+1);

    if(!buffer)
    {
        fclose(fp);
        return false;
    }

    if (buffer)
    {
        fread(buffer, sz, 1, fp);
        fclose(fp);
    }

    json_char* json;
    json_value* value;

    json = (json_char*)buffer;

    value = json_parse(json, strlen(buffer));

    for (int index = 0; index < value->u.object.length; index++)
    {
        json_value* val = value->u.object.values[index].value;
        char* name = value->u.object.values[index].name;

        switch (val->type)
        {
            case json_string:
            {
                if(strcmp(name, "device_id") == 0)
                {
                    strcpy(device_id, val->u.string.ptr);
                }

                if(strcmp(name, "destination") == 0)
                {
                    strcpy(destination, val->u.string.ptr);
                }

                if(strcmp(name, "name") == 0)
                {
                    strcpy(plugin_name, val->u.string.ptr);
                }

                if(strcmp(name, "device") == 0)
                {
                    strcpy(plugin_device, val->u.string.ptr);
                }

                if(strcmp(name, "resource") == 0)
                {
                    strcpy(resource_files, val->u.string.ptr);
                }

                if(strcmp(name, "callback_frequency") == 0)
                {
                    callback_frequency = atol(val->u.string.ptr);
                }

                if(strcmp(name, "roi_x1") == 0)
                {
                    roi_x1 = atol(val->u.string.ptr);
                }

                if(strcmp(name, "roi_y1") == 0)
                {
                    roi_y1 = atol(val->u.string.ptr);
                }

                if(strcmp(name, "roi_x2") == 0)
                {
                    roi_x2 = atol(val->u.string.ptr);
                }

                if(strcmp(name, "roi_y2") == 0)
                {
                    roi_y2 = atol(val->u.string.ptr);
                }

                if(strcmp(name, "threshold_score") == 0)
                {
                    threshold_score = atof(val->u.string.ptr);
                }

                break;
            }

            default:
            {

            }
        }
    }


    return true;
}
