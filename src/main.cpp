#include <limits.h>
#include <memory.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include <pthread.h>
#include <opencv2/opencv.hpp>
#include <tensorflow/c/c_api.h>
#include <algorithm>
#include <numeric>
#include <math.h>
#include <fstream>
#include <utility>
#include <vector>
#include <iostream>
#include <regex>

#include "Defines.hpp"
#include "Buffer.hpp"
#include "Base64.hpp"
#include "Directory.hpp"
#include "Environment.hpp"
#include "File.hpp"
#include "Logger.hpp"
#include "MessageBus.hpp"
#include "StringEx.hpp"
#include "SignalHandler.hpp"
#include "AppConfig.hpp"

typedef enum ResultType
{
    Measurement = 0,
    VideoEvent=1,
    Summary=2,
    ImageData=3
}ResultType;

typedef struct detector_result
{
    ResultType result;
    char name[65];
    float resultvaluef;
    float fps;
    unsigned long resultvaluel;
    unsigned long timestamp;
    char* base64imagedata;
    char* kvdata;
}detector_result;

typedef void(*va_callback)(const detector_result* result);

static bool loadLabels(std::map<int, std::string> &lmp);
static bool loadGraph(TF_Graph** graph);
static bool initialize();
static bool configureDecoder(va_callback callback, unsigned int callback_frequency_seconds, const char* name, const char* device, const char* resource);
static bool runDecoder();
static void ipcCallback(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id);
static void detectorCallback(const detector_result* result);
static void signalCallback(SignalType type);
static void deallocateTensor(void* data, std::size_t, void*);

static logger_t* logger = nullptr;
static message_bus_t* mbus = nullptr;
static va_callback pcallback = nullptr;
static bool continue_processing = true;
static bool debug = false;

int main(int argc, char* argv[])
{
    if(argc == 2)
    {
        if(argv[1] != nullptr)
        {
            if(strcmp(argv[1], "-d") == 0)
            {
                debug = true;
            }
        }
    }

    if(!initialize())
    {
        return -1;
    }


    if(!configureDecoder(detectorCallback, callback_frequency, plugin_name, plugin_device, resource_files))
    {
        return -1;
    }

    bool ret = runDecoder();
    return (int)ret;
}

bool initialize()
{
    logger = logger_allocate_default();

    if(logger == nullptr)
    {
        return false;
    }

    signals_register_callback(signalCallback);
    signals_initialize_handlers();

    mbus = message_bus_initialize(ipcCallback);

    if(!mbus)
    {
        WriteLog(logger, "Could not intialize IPC", LOG_ERROR);
        return false;
    }

    if(!message_bus_open(mbus))
    {
        WriteLog(logger, "Could not open IPC", LOG_ERROR);
        return false;
    }

    return loadConfiguration();
}

bool configureDecoder(va_callback callback, unsigned int callback_frequency_seconds, const char *name, const char *device, const char *resource)
{
    if(resource)
    {
        char** tokens = strsplitchar(resource, ',');
        char temp[128] = {0};
        dir_get_config_directory(temp);
        strcpy(model_file, strrepsubstrfirst(temp, "/etc", "/share"));
        strcpy(label_file, strrepsubstrfirst(temp, "/etc", "/share"));
        strcat(model_file, tokens[0]);
        strcat(label_file, tokens[1]);
        strfreelist(tokens);
    }

    callback_frequency = callback_frequency_seconds;
    pcallback = callback;

    return true;
}

void ipcCallback(const char* node_name, PayloadType ptype, DataType dtype, const char* messagebuffer, long buffersize, long payload_id)
{

}

void signalCallback(SignalType stype)
{
    switch(stype)
    {
        case Suspend:
        {
            WriteLog(logger, "SUSPEND SIGNAL", LOG_CRITICAL);
            break;
        }
        case Resume:
        {
            WriteLog(logger, "RESUME SIGNAL", LOG_CRITICAL);
            break;
        }
        case Shutdown:
        {
            WriteLog(logger, "SHUTDOWN SIGNAL", LOG_CRITICAL);
            continue_processing = false;
            break;
        }
        case Alarm:
        {
            WriteLog(logger, "ALARM SIGNAL", LOG_CRITICAL);
            break;
        }
        case Reset:
        {
            WriteLog(logger, "RESET SIGNAL", LOG_CRITICAL);
            break;
        }
        case ChildExit:
        {
            WriteLog(logger, "CHILD PROCESS EXIT SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined1:
        {
            WriteLog(logger, "USER DEFINED 1 SIGNAL", LOG_CRITICAL);
            break;
        }
        case Userdefined2:
        {
            WriteLog(logger, "USER DEFINED 2 SIGNAL", LOG_CRITICAL);
            break;
        }
        case WindowResized:
        {
            WriteLog(logger, "WINDOW SIZE CHANGED SIGNAL", LOG_CRITICAL);
            break;
        }
        default:
        {
            WriteLog(logger, "UNKNOWN SIGNAL", LOG_CRITICAL);
            break;
        }
    }
}

void detectorCallback(const detector_result* result)
{
    buffer_t* payload = buffer_allocate_default();

    buffer_append_char(payload, '{');

    buffer_append_char(payload, '"');
    //This key needs to be renamed to "device_id"
    //buffer_append_string(payload, "device_id");
    buffer_append_string(payload, "peripheral_id");
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ':');
    buffer_append_char(payload, '"');
    buffer_append_string(payload, device_id);
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ',');

    buffer_append_char(payload, '"');
    //This key needs to be renamed to "ts"
    //buffer_append_string(payload, "ts");
    buffer_append_string(payload, "timestamp");
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ':');
    buffer_append_unix_timestamp_ms(payload);
    buffer_append_char(payload, ',');

    //////////////////////////////

    // Detector
    buffer_append_char(payload, '"');
    buffer_append_string(payload, "detector");
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ':');
    buffer_append_char(payload, '"');
    buffer_append_string(payload, result->name);
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ',');

    // Type
    buffer_append_char(payload, '"');
    buffer_append_string(payload, "type");
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ':');
    buffer_append_char(payload, '"');

    if(result->result == Measurement)
    {
        buffer_append_string(payload, "measurement");
    }

    if(result->result == VideoEvent)
    {
        buffer_append_string(payload, "video_event");
    }

    if(result->result == Summary)
    {
        buffer_append_string(payload, "event_summary");
    }

    buffer_append_char(payload, '"');
    buffer_append_char(payload, ',');

    // Data
    buffer_append_char(payload, '"');
    buffer_append_string(payload, "data");
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ':');

    if(result->result == Measurement)
    {
        buffer_append_real(payload, result->resultvaluef);
    }

    if(result->result == VideoEvent)
    {
        buffer_append_integer(payload, result->resultvaluel);
    }

    if(result->result == Summary)
    {
        buffer_append_char(payload, '"');
        buffer_append_string(payload, result->kvdata);
        buffer_append_char(payload, '"');
    }

    if(result->result == ImageData)
    {
        buffer_append_char(payload, '"');
        buffer_append_string(payload, result->base64imagedata);
        buffer_append_char(payload, '"');
    }
    buffer_append_char(payload, ',');

    // FPS
    buffer_append_char(payload, '"');
    buffer_append_string(payload, "fps");
    buffer_append_char(payload, '"');
    buffer_append_char(payload, ':');
    buffer_append_real(payload, result->fps);

    //////////////////////////////

    buffer_append_char(payload, '}');

    if (!message_bus_send(mbus, destination, Data, Text, (char*)buffer_get_data(payload), (long)buffer_get_size(payload)))
    {
        WriteLog(logger, "IPC failed -  could not send payload", LOG_ERROR);
        WriteLog(logger, (char*)buffer_get_data(payload), LOG_ERROR);
    }

    std::cout << (char*)buffer_get_data(payload) << std::endl;

    buffer_free(payload);
}

bool loadLabels(std::map<int, std::string> &lmp)
{
    // Read file into a string
    std::ifstream t(label_file);
    if (t.bad())
    {
        return false;
    }

    std::stringstream buffer;
    buffer << t.rdbuf();
    std::string fileString = buffer.str();

    // Search entry patterns of type 'item { ... }' and parse each of them
    std::smatch matcherEntry;
    std::smatch matcherId;
    std::smatch matcherName;
    const std::regex reEntry("item \\{([\\S\\s]*?)\\}");
    const std::regex reId("[0-9]+");
    const std::regex reName("\'.+\'");
    std::string entry;

    auto stringBegin = std::sregex_iterator(fileString.begin(), fileString.end(), reEntry);
    auto stringEnd = std::sregex_iterator();

    int id;
    std::string name;

    for (std::sregex_iterator i = stringBegin; i != stringEnd; i++)
    {
        matcherEntry = *i;
        entry = matcherEntry.str();
        regex_search(entry, matcherId, reId);

        if (!matcherId.empty())
         {
            id = stoi(matcherId[0].str());
        }
        else
        {
            continue;
        }

        regex_search(entry, matcherName, reName);

        if (!matcherName.empty())
        {
            name = matcherName[0].str().substr(1, matcherName[0].str().length() - 2);
        }
        else
        {
            continue;
        }

        lmp.insert(std::pair<int, std::string>(id, name));
    }

    return true;
}

bool loadGraph(TF_Graph **graph)
{
    std::ifstream f(model_file, std::ios::binary);

    if (f.fail() || !f.is_open())
    {
      return false;
    }

    if (f.seekg(0, std::ios::end).fail())
    {
      return false;
    }

    size_t fsize = f.tellg();

    if (f.seekg(0, std::ios::beg).fail())
    {
      return false;
    }

    if (fsize <= 0)
    {
      return false;
    }

    char* data = static_cast<char*>(std::malloc(fsize));

    if (f.read(data, fsize).fail())
    {
        free(data);
        return false;
    }

    TF_Buffer* buf = TF_NewBuffer();
    buf->data = data;
    buf->length = fsize;

    *graph = TF_NewGraph();
    TF_ImportGraphDefOptions* opts = TF_NewImportGraphDefOptions();

    TF_Status* status = TF_NewStatus();
    TF_GraphImportGraphDef(*graph, buf, opts, status);
    TF_DeleteImportGraphDefOptions(opts);
    TF_DeleteBuffer(buf);
    TF_DeleteStatus(status);
    free(data);

    return true;
}

bool runDecoder()
{
    detector_result* result = (detector_result*)calloc(1, sizeof(detector_result));

    if(!result)
    {
        return false;
    }

    //Initialive the Result object
    memset(result->name, 0, 65);
    strcpy(result->name, "led_detection_event");
    result->base64imagedata = nullptr;
    result->result = Summary;

    //Initialize the TensforFlow objects
    std::map<int, std::string> labelsMap = std::map<int,std::string>();
    TF_Session* session = nullptr;
    TF_Graph* graph = nullptr;
    std::vector<TF_Output> input_ops;
    std::vector<TF_Output> output_ops;
    std::vector<TF_Tensor*> inputtensor;
    TF_Tensor* outputtensors = nullptr;
    TF_Status* status = nullptr;
    TF_SessionOptions* options = nullptr;
    std::map<int, int> ledStateCount = std::map<int,int>();

    //Load the labels
    if(!loadLabels(labelsMap))
    {
        return false;
    }

    //Load the graph
    if(!loadGraph(&graph))
    {
        return false;
    }

    //Define input layers
    auto input_image = TF_Output{TF_GraphOperationByName(graph, "image_tensor:0"), 0};
    input_ops.push_back(input_image);

    //Define output layers
    auto boxes = TF_Output{TF_GraphOperationByName(graph, "detection_boxes:0"), 0};
    output_ops.push_back(boxes);
    auto scores = TF_Output{TF_GraphOperationByName(graph, "detection_scores:0"), 0};
    output_ops.push_back(scores);
    auto classes = TF_Output{TF_GraphOperationByName(graph, "detection_classes:0"), 0};
    output_ops.push_back(classes);
    auto num_detect = TF_Output{TF_GraphOperationByName(graph, "num_detections:0"), 0};
    output_ops.push_back(num_detect);

    //Initialize the labels map with zero count
    std::map<int, std::string>::iterator it;
    for(it = labelsMap.begin(); it != labelsMap.end(); ++it)
    {
        ledStateCount[it->first] = 0;
    }

    // FPS count and frame calculation variable
    unsigned long nFrames = 25;
    unsigned long iFrame = 0;
    unsigned long summary_count = 0;
    unsigned long totalCnt = 0;
    unsigned long cnt = 0;
    double thresholdIOU = 0.8;
    int64_t frame_width = 0;
    int64_t frame_height = 0;
    int64_t channels = 0;

    //Open the video source and
    cv::VideoCapture capture(plugin_device);
    //Initialize OpenCV objects
    cv::Mat frame;
    cv::Mat selectedRegion;

    frame_width = (int64)capture.get(cv::CAP_PROP_FRAME_WIDTH);
    frame_height = (int64)capture.get(cv::CAP_PROP_FRAME_HEIGHT);

    //Set up timers
    time_t start, end;
    time(&start);
    time(&end);

    std::vector<size_t> detected;

    while (capture.read(frame))
    {
        if(frame.empty())
        {
            continue;
        }

        cvtColor(frame, frame, cv::COLOR_BGR2RGB);

        //Cut out the segment of the frame that we want to analyze
        cv::Rect2d rect(roi_x1, roi_y1, (roi_x2-roi_x1), (roi_y2-roi_y1));

        selectedRegion = frame(rect);

        //If debug is set, then display the frame
        if(debug)
        {
            cv::imshow("Debug", selectedRegion);
            if(cv::waitKeyEx(1) == 'q')
            {
                break;
            }
        }

        double seconds = difftime (end, start);

        if (nFrames % (iFrame + 1) == 0)
        {
            time(&end);
            result->fps = 1. * nFrames / seconds;
            time(&start);
        }
        iFrame++;

        //Convert OpenCV Mat to Tensor
        channels = selectedRegion.channels();

        int rows = selectedRegion.rows;
        int cols = selectedRegion.cols;
        int num_el = rows*cols;
        int len = num_el*selectedRegion.elemSize1();

        int64_t dims[] = {1, frame_width, frame_height, channels };
        TF_Tensor * imgTensor = TF_NewTensor(TF_FLOAT, dims, 4, (void*)selectedRegion.data, len*sizeof(float), &deallocateTensor, nullptr);
        inputtensor.push_back(imgTensor);

        // Run the graph on tensor after preparing the session

        //Status
        status = TF_NewStatus();

        //Options
        options = TF_NewSessionOptions();

        //Session
        session = TF_NewSession(graph, options, status);

        TF_SessionRun(session,
                      nullptr, // Run options.
                      output_ops.data(), inputtensor.data(), 1, // Input tensors, input tensor values, number of inputs.
                      output_ops.data(), &outputtensors, 1, // Output tensors, output tensor values, number of outputs.
                      nullptr, 0, // Target operations, number of targets.
                      nullptr, // Run metadata.
                      status // Output status.
                      );

        if (TF_GetCode(status) != TF_OK)
        {
            continue;
        }

        // Extract results from the outputs vector
        std::vector<size_t> goodIdxs;

        //tensorflow::TTypes<float>::Flat scores = outputs[1].flat<float>();
        //tensorflow::TTypes<float>::Flat classes = outputs[2].flat<float>();
        //tensorflow::TTypes<float>::Flat numDetections = outputs[3].flat<float>();
        //tensorflow::TTypes<float, 3>::Tensor boxes = outputs[0].flat_outer_dims<float,3>();

        //std::vector<size_t> goodIdxs = filterBoxes(scores, boxes, thresholdIOU, threshold_score);

        for(int ctr = 0; ctr < goodIdxs.size(); ctr++)
        {
            // Old score
            int state_score =  ledStateCount[goodIdxs[ctr]+1];
            // New score
            state_score++;
            ledStateCount[goodIdxs[ctr]+1] = state_score;
        }

        if(seconds >= callback_frequency)
        {
            std::map<int, int>::iterator lit;
            std::string result_str;

            for(lit = ledStateCount.begin(); lit != ledStateCount.end(); ++lit)
            {
                char buff[10] = {0};
                sprintf(buff, "%d", lit->second);
                result_str += labelsMap[lit->first] + ":" + buff + ",";
            }

            result_str.pop_back();
            result->timestamp = (unsigned long)time(nullptr);

            if(pcallback)
            {
                result->kvdata = (char*)result_str.c_str();

                pcallback(result);

                result->fps = 0;

                for(it = labelsMap.begin(); it != labelsMap.end(); ++it)
                {
                    ledStateCount[it->first] = 0;
                }
            }

            totalCnt = 0;
            time(&start);
        }

        for( auto ts : inputtensor)
        {
            TF_DeleteTensor(ts);
        }

        inputtensor.clear();

        TF_DeleteSessionOptions(options);
        TF_DeleteSession(session, status);
        TF_DeleteStatus(status);
        session = nullptr;
        status = nullptr;
        options = nullptr;

        time(&end);
    }

    return true;
}

void deallocateTensor(void* data, std::size_t, void*)
{
  std::free(data);
}
