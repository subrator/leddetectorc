/*

Copyright (c) 2020, CIMCON Software India Pvt. Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef STRING_EX_C
#define STRING_EX_C

#include <wchar.h>
#include "Defines.hpp"

#ifdef __cplusplus
extern "C" {
#endif

extern wchar_t* strtowstr(const char* str);
extern char* strfromwstr(const wchar_t* wstr);

extern char* strfromint(long num);
extern char* strfromdouble(double num);

extern char* strreverse(char* ptr);
extern char* strsegmentreverse(char* str, long start, long term);

extern long strindexofsubstr(const char *str, const char* substr);
extern long strindexofchar(const char *str, const char ch);

extern long strcountsubstr(const char *str, const char* substr);
extern long strcountchar(char* str, const char ch);

extern char* strtolower(char* str);
extern char* strtoupper(char* str);

extern char* strlefttrim(char* str);
extern char* strrighttrim(char* str);
extern char* stralltrim(char* str);

extern char* strremsubstrfirst(char* str, const char* substr);
extern char* strremsubstrall(char* str, const char* substr);
extern char* strremsubstrat(char* str, long pos, long len);

extern char* strremcharfirst(char* str, const char oldchar);
extern char* strremcharall(char* str, const char oldchar);
extern char* strremcharat(char* str, long pos);

extern char* strrepsubstrfirst(char* str, const char* oldsubstr, const char* newsubstr);
extern char* strrepsubstrall(char* str, const char* oldsubstr, const char* newsubstr);

extern char* strrepcharfirst(char* str, const char oldchar, const char newchar);
extern char* strrepcharall(char* str, const char oldchar, const char newchar);
extern char* strrepcharat(char* str, const char newchar, long pos);

extern void strsplitkeyvaluechar(const char* str, const char delimiter, char **key, char **value);
extern void strsplitkeyvaluesubstr(const char* str, const char* delimiter, char **key, char **value);
extern char** strsplitsubstr(const char* str, const char* delimiter);
extern char** strsplitchar(const char* str, const char delimiter);
extern char* strjoinwithsubstr(const char **strlist, const char* delimiter);
extern char* strjoinwithchar(const char** strlist, const char delimiter);
extern void  strfreelist(char** strlist);

#ifdef __cplusplus
}
#endif

#endif
