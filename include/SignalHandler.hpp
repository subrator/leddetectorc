/*

Copyright (c) 2020, CIMCON Software India Pvt. Ltd.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is allowed only with prior permission from CIMCON Automation

*/

#ifndef SIGNAL_HANDLER_C
#define SIGNAL_HANDLER_C

#include "Defines.hpp"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum SignalType
{
    Suspend=0,
    Resume=1,
    Shutdown=2,
    Alarm=3,
    Reset=4,
    ChildExit=5,
    Userdefined1=6,
    Userdefined2=7,
    WindowResized=8
}SignalType;

typedef void(*signal_callback)(SignalType stype);

extern void signals_initialize_handlers();
extern void signals_register_callback(signal_callback callback_func);
extern bool signals_is_shutdownsignal(const int signum);
extern void signals_get_name(const int signum);

#ifdef __cplusplus
}
#endif

#endif
