cmake_minimum_required(VERSION 3.5)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(APP_VERSION_MAJOR 1)
set(APP_VERSION_MINOR 0)
set(APP_VERSION_PATCH 0)

set(Project LEDDetectorC)
project(${Project})

include_directories(${PROJECT_HEADER_DIR} /include/ /usr/include/ /usr/local/include/ ./include ../include /usr/include/)
link_libraries(rt pthread dl ssl crypto mosquitto opencv_core opencv_videoio opencv_objdetect opencv_imgproc opencv_highgui opencv_face opencv_shape tensorflow tensorflow_framework)
link_directories(./lib/ /lib /usr/lib/ /usr/local/lib/ /usr/lib/${CMAKE_SYSTEM_PROCESSOR}/)

set(SOURCE
${SOURCE}
./src/Buffer.cpp
./src/Base64.cpp
./src/Directory.cpp
./src/Logger.cpp
./src/StringEx.cpp
./src/Environment.cpp
./src/File.cpp
./src/MessageBus.cpp
./src/AppConfig.cpp
./src/Json.cpp
./src/SignalHandler.cpp
./src/main.cpp
)

set(HEADERS
${HEADERS}
./include/Defines.hpp
./include/Buffer.hpp
./include/Base64.hpp
./include/Directory.hpp
./include/Logger.hpp
./include/StringEx.hpp
./include/Environment.hpp
./include/File.hpp
./include/MessageBus.hpp
./include/AppConfig.hpp
./include/Json.hpp
./include/SignalHandler.hpp
)

add_executable(app_led_detector_c ${SOURCE} ${HEADERS})

